<?php

// web/index.php
require_once __DIR__.'/../vendor/autoload.php';

use Silex\Application;

/** @var Application $app */
$app = require __DIR__.'/../src/app.php';

require __DIR__.'/../src/controllers.php';

$app->run();