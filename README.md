# Acme Payments API

Acme Payments is creating a new API to allow customers to save payment accounts so that 
they do not have to enter their payment details every time they make a purchase.  

Currently there are some endpoint available for saving new account data and retrieving a stored account.  
However, testing has reveled some security issues and we would like refactor this into a more OOP 
approach so that the business logic can be made available to other parts of the application.  

## Instructions

Fork this project to your own repository.  Create an Account model, repository, and (container) service that 
can be used by the application endpoints to perform the needed tasks. Keep in mind that in the future 
additional repository types may be needed.  Note that, while the Silex framework is being used to handle 
the application request, all code containing business logic should remain framework agnostic.  Be sure to 
include any relevant unit tests.  
 
*Note: While the subject matter found herein deals with sensitive data, the topics of 
encryption and data sanitation are not in the scope of this project. Any use of this code, or 
extension of it should only be used for demonstrational purposes only.*

## Setup

First install the needed [Composer](https://getcomposer.org/) packages:  

```composer install```

You can then start a web server for the project by running the following command from the project root
directory.  This step is not absolutely required.  Application logic should be testable without the 
availability of a running web server.

```php -S localhost:8888 -t web```

## Structure

The current project uses the [Silex micro-framework](http://silex.sensiolabs.org), with 
the current code structure:

* ```bin/``` - Hold any executable files from installed Composer packages 
* ```data/``` - Database table schema
* ```src/``` - contains application specific code 
  *  ```Acme``` - Namespace folder for project source code. 
  *  ```src/app.php``` - Silex application bootstrap file.  Contains DB config details. 
  Custom services can be defined here.
  *  ```src/controllers.php``` - Routing file for API endpoints 
* ```tests/``` - The tests folder contains unit tests for source code located in ```src/```
* ```vendor/``` - Composer packages
* ```web/``` - Web request entry point.



