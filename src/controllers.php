<?php

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Find an account based on the system assigned ID.
 * Returns JSON formatted Account object.
 *
 * Example:
 * GET http://localhost:8888/accounts/123
 *
 */
$app->get('/accounts/{id}', function ($id) use ($app) {

    $stmt = $app['db']->executeQuery("SELECT * FROM payment_accounts WHERE id = {$id};");

    if (!$stmt) {
        return new JsonResponse(['error'=>'There was an error with your request.'], 400);
    }

    $account = $stmt->fetch();

    if (!$account) {
        return new JsonResponse(['error'=>'Account not found.'], 400);
    }

    if ($account['type'] == 'card') {
        unset($account['routing_number']);
    }
    else if ($account['type'] == 'bank') {
        unset($account['expiration']);
    }

    return new JsonResponse($account);
});

/**
 * Create a new account.
 *
 * Accepts data posted in application/json format.
 * Returns JSON formatted Account object.
 *
 * Examples:
 *
 * POST http://localhost:8888/accounts
 * Content-Type: application/json
 * {
 *   "type": "card",
 *   "account_number": "4111111111111111",
 *   "expiration": "1221"
 * }
 *
 * POST http://localhost:8888/accounts
 * Content-Type: application/json
 * {
 *   "type": "bank",
 *   "account_number": "112233",
 *   "routing_number": "123123123"
 * }
 */
$app->post('/accounts', function (Request $request) use ($app) {
    $data = json_decode($request->getContent(), true);

    $currentTimestamp = date('Y-m-d h:i:s');

    $account = [
        'type' => $data['type'],
        'created_at' => $currentTimestamp,
        'updated_at' => $currentTimestamp
    ];

    $stmt = null;

    if ($data['type'] == 'card') {
        $stmt = $app['db']->executeQuery("
          INSERT INTO payment_accounts (type, account_number, routing_number, expiration, created_at, updated_at )
          VALUES ('card', '".$data['account_number']."', '', '".$data['expiration']."', '{$currentTimestamp}', '{$currentTimestamp}')
        ");

        $account['account_number'] = $data['account_number'];
        $account['expiration'] = $data['expiration'];

    }
    else if ($data['type'] == 'bank') {
        $stmt = $app['db']->executeQuery("
          INSERT INTO payment_accounts (type, account_number, routing_number, expiration, created_at, updated_at )
          VALUES ('bank', '".$data['account_number']."', '".$data['routing_number']."', '', '{$currentTimestamp}', '{$currentTimestamp}')
        ");

        $account['account_number'] = $data['account_number'];
        $account['routing_number'] = $data['routing_number'];
    }

    if (!$stmt) {
        return new JsonResponse(['error'=>'There was an error with your request.'], 400);
    }

    $account['id'] = $app['db']->lastInsertId();

    return new JsonResponse($account);
});