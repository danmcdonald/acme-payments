<?php

$app = new Silex\Application();

$app['debug'] = true;

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_mysql',
        'host'      => 'localhost',
        'dbname'    => 'sandbox',
        'user'      => 'root',
        'password'  => '0000',
    ),
));

// TODO: Add Custom Services

// $app['some_service'] = function () use ($app) {
//     return new Service();
// };


return $app;